# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.23)
# Database: wunderlite
# Generation Time: 2018-10-14 13:45:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `addresses`;

CREATE TABLE `addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_user_id_foreign` (`user_id`),
  CONSTRAINT `addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;

INSERT INTO `addresses` (`id`, `street`, `house_number`, `zip_code`, `city`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,'12 John Street','34','10001','New York',1,'2018-10-14 12:54:24','2018-10-14 12:54:24'),
	(2,'12 John Street','78','10001','New York',2,'2018-10-14 12:55:40','2018-10-14 12:55:40'),
	(3,'12 John Street','24','10001','New York',3,'2018-10-14 13:15:54','2018-10-14 13:15:54'),
	(4,'12 John Street','34','10001','New York',4,'2018-10-14 13:17:11','2018-10-14 13:17:11'),
	(5,'12 John Street','56','10001','New York',6,'2018-10-14 13:18:32','2018-10-14 13:18:32');

/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(43,'2014_10_12_000000_create_users_table',1),
	(44,'2018_10_07_211053_create_addresses_table',1),
	(45,'2018_10_07_211812_create_payments_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table payments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iban` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payments_user_id_foreign` (`user_id`),
  CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;

INSERT INTO `payments` (`id`, `account_name`, `iban`, `payment_id`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,'refghfgdh  hfdh','34634634','d6134fd0dae392b07fa604439bade47aa14e4c580fc35a269800fe362809df7c768896674e6b5d2deb247d13aa71e670',1,'2018-10-14 12:54:31','2018-10-14 12:54:32'),
	(2,'refghfgdh  hfdh','3523452345','69c3ad40c8e640212518f1a19ec4f2544063f01c211cd987fea74e6d0d89a92d1eda2450913142dcef51cf38c658477a',2,'2018-10-14 12:55:46','2018-10-14 12:55:46'),
	(3,'dfgsdfgsdfgsdfg  ddsfgsdfgdf','1563463463','a85f144cbe5843f5dde5c59bb828b06ae5c473604f8a2adbf7c18e7252a0f3d5898b0b968307cc7ec9f2f359073db690',3,'2018-10-14 13:16:02','2018-10-14 13:16:03'),
	(4,'reterwtewrtv egdfgsdgsd','523523452345','9aecd21b93e60b4234b1ef7f793bf4890232ba0036ad32a5c139236d6129ec4238fa73a7c386ece9bb50ef7c50d3b328',4,'2018-10-14 13:17:16','2018-10-14 13:17:16'),
	(5,'reterwtewrtv egdfgsdgsd','3523452345','10fc857b212da5b7cc294fef9ee9fd7585da054dd1b356298482a992c884d78df691b8616c370fdc7791ba8bf4ef3869',6,'2018-10-14 13:18:37','2018-10-14 13:18:38');

/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_telephone_unique` (`telephone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `first_name`, `last_name`, `telephone`, `created_at`, `updated_at`)
VALUES
	(1,'Akande','Surajudeen','9292383469','2018-10-14 12:54:18','2018-10-14 12:54:18'),
	(2,'Akande','Surajudeen','92923834695252','2018-10-14 12:55:33','2018-10-14 12:55:33'),
	(3,'Akande','Surajudeen','9292383469636','2018-10-14 13:15:48','2018-10-14 13:15:48'),
	(4,'Akande','Surajudeen','929238346912','2018-10-14 13:17:06','2018-10-14 13:17:06'),
	(6,'Akande','Surajudeen','92923834693636','2018-10-14 13:18:26','2018-10-14 13:18:26');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
