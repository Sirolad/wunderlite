$(document).ready(function () {
  let stepOne = $('#step-one');
  let stepTwo = $('#step-two');
  let stepThree = $('#step-three');

  if (stepTwo.is(':visible')) {
    $('#b2').removeAttr('disabled');
    $('#b1').attr('disabled', true)
  }

  if (stepThree.is(':visible')) {
    $('#b3').removeAttr('disabled');
    $('#b2').attr('disabled', true);
    $('#b1').attr('disabled', true)
  }

  stepOne.submit(function (e) {
    e.preventDefault();

    $.ajax({
      type: "POST",
      url: "/step1",
      data: {
        _token: $(this).attr('data-token'),
        first_name: $('#first-name').val(),
        last_name: $('#last-name').val(),
        telephone: $('#telephone').val()
      },
      success: function (response) {
        if (response.success) {
          window.location.reload();
        }
      },
      error: function (err) {
        alert(err.responseJSON.message)
      }
    })
  });

  stepTwo.submit(function (e) {
    e.preventDefault();

    $.ajax({
      type: "POST",
      url: "/step2",
      data: {
        _token: $(this).attr('data-token'),
        street: $('#street').val(),
        house_number: $('#house').val(),
        zip_code: $('#zip_code').val(),
        city: $('#city').val(),
      },
      success: function (response) {
        if (response.success) {
          window.location.reload();
        }
      },
      error: function (err) {
        alert(err.responseJSON.message)
      }
    })
  });
});