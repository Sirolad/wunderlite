<?php

namespace WunderLite\Services;

use GuzzleHttp\Client;
use WunderLite\Payment;
use GuzzleHttp\Exception\RequestException;


class PaymentService
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * PaymentService constructor.
     * @param Client $client
     *
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param Payment $payment
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPaymentId(Payment $payment)
    {
        try {
            $request = $this->client->request("POST", env('PAYMENT_SERVER'), [
                'json' => [
                    'customerId' => 1,
                    'iban' => $payment->iban,
                    'owner' => $payment->account_name
                ]
            ]);

            $response = json_decode($request->getBody());
        } catch (RequestException $e) {
            return \Log::info($e->getMessage());
        }

        $id = $response->paymentDataId;

        $payment->payment_id = $id;
        $payment->save();

        return [
            'success' => true,
            'id'      => $id
        ];
    }
}