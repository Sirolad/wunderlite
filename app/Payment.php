<?php

namespace WunderLite;

use Illuminate\Database\Eloquent\Model;
use WunderLite\Events\PaymentDetailsSaved;

class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_name', 'iban', 'payment_id', 'user_id'
    ];
}
