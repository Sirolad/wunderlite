<?php

namespace WunderLite;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street', 'house_number', 'zip_code', 'city', 'user_id'
    ];
}
