<?php

namespace WunderLite\Http\Controllers;

use WunderLite\Payment;
use Illuminate\Http\Request;
use WunderLite\Services\PaymentService as Service;

class PaymentController extends Controller
{
    protected $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function stepThree(Request $request)
    {
        $model = $this->create($request);

        $getId = $this->service->getPaymentId($model);

        $request->session()->flush();

        if ($getId['success']) {
            return view('success' , ['id'=> $getId['id']]);
        }

    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function create(Request $request)
    {
        return Payment::create(
            [
                'account_name' => $request->account_name,
                'iban' => $request->iban,
                'user_id' => $request->session()->get('step-two-done')
            ]
        );
    }
}
