<?php

namespace WunderLite\Http\Controllers;

use WunderLite\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function stepTwo(Request $request)
    {
        if ($this->create($request)){
            $request->session()->put('step-two-done', $request->session()->get('step-one-done'));
            return response()->json([
                'success' => true,
            ], 200);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function create(Request $request)
    {
        return Address::create(
            [
                'street' => $request->street,
                'house_number' => $request->house_number,
                'zip_code' => $request->zip_code,
                'city' => $request->city,
                'user_id' => $request->session()->get('step-one-done')
            ]
        );
    }
}
