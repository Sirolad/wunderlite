## Possible Performance Optimizations
* Use Redis or Memcached instead of filesystem for improved session performance.
* Server Side Rendering could be used to improve how the application is served to the users.
* Static files could be on a CDN for fast delivery to users.
* There are specific configuration that can be used to improve the database server

## Things that could be done better     
* Write Unit Tests with high code coverage.
* Validate data both in frontend and backend.
* Use a user-friendly alert for notification for errors instead of the JS alert().
