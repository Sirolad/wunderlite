# WunderLite Registration Application

[![License](http://img.shields.io/:license-mit-blue.svg)](https://github.com/sirolad/Wunderlite/blob/master/LICENSE)

WunderLite is a mini WunderFleet Application that enable users to register. A demo of the app is live [here](https://wunderlite.herokuapp.com)
The app is built with Laravel.


## Usage

To download and use this project you might want to have the following installed on your machine but any stable 
locate development setup for LAMP should work.

- Composer
  Visit the [official website](https://getcomposer.org/doc/00-intro.md) for installation instructions.
- Laravel Valet
  Visit [Laravel website](https://laravel.com/docs/5.5/valet) for installation and setup instructions.

When you have completed the above processes, run:

```bash
$ git clone https://github.com/sirolad/WunderLite
```
to clone the repository to your working directory. This step presumes that you have git set up and running.

Run

```bash
$ cd WunderLite
$ composer install
```
to pull in the project dependencies.
```php
$ cp env.example .env
```
Create a database and fill the .env based on your local configuration.
```php
$ php artisan migrate
``` 

Start your server and check the app on your browser.

Now you are set up and ready to run.

## App features
- Persists User's Registration Data
- Allow users to continue at any point in time incase they stopped could not complete the forms in one sitting.
- Complete payment transaction and get payment ID from Wunder.

## Database Export

This is available in [here](wunderlite.sql)

## Improvements

Read improvements [here](improvement.md)

## Testing

``` bash
$ phpunit
```

## Credits

WunderLite is maintained by `Surajudeen AKANDE`.

## License

WunderLite is released under the MIT Licence. See the bundled [LICENSE](LICENSE) file for details.