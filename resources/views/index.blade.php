@extends('layout')

@section('content')
    @include('step-wizard')
    @if(session()->has('step-one-done') && !session()->has('step-two-done'))
        @include('forms.steptwo')
    @elseif(session()->has('step-two-done'))
        @include('forms.stepthree')
    @else
        @include('forms.stepone')
    @endif
@endsection
