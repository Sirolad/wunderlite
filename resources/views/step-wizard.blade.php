<div>
    <b>WunderLite Registration</b>
</div>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" id="b1" type="button" class="btn btn-primary btn-circle">1</a>
            <p>Step 1</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" id="b2" type="button" class="btn btn-primary btn-circle" disabled="disabled">2</a>
            <p>Step 2</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" id="b3" type="button" class="btn btn-primary btn-circle" disabled="disabled">3</a>
            <p>Step 3</p>
        </div>
    </div>
</div>