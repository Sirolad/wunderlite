@extends('layout')

@section('content')
    <h4>Your Registration was successful, your payment Id is :</h4>
    <div class="jumbotron-fluid">
        <code>{{ $id }}</code>
    </div>
    <br>
    <a class="btn-lg btn-primary" href="/">Register a New User here</a>
@endsection
