<form role="form" id="step-three" method="POST" action="{{ route('stepThree') }}">
    @csrf
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 3</h3>
                <div class="form-group">
                    <label class="control-label">Account Name</label>
                    <input maxlength="200" type="text" required="required" id="account_name" name="account_name" class="form-control" placeholder="Enter Account Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">IBAN</label>
                    <input maxlength="200" type="text" required="required" id="iban" name="iban" class="form-control" placeholder="Enter IBAN Number"  />
                </div>
                <button class="btn btn-success btn-lg pull-right" type="submit">Finish!</button>
            </div>
        </div>
    </div>
</form>