<form role="form" id="step-one" data-token="{{ csrf_token() }}">
    <div class="row setup-content">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 1</h3>
                <div class="form-group">
                    <label class="control-label">First Name</label>
                    <input  maxlength="100" type="text" required="required" id="first-name" name="first-name" class="form-control" placeholder="Enter First Name"  />
                </div>
                <div class="form-group">
                    <label class="control-label">Last Name</label>
                    <input maxlength="100" type="text" required="required" id="last-name" name="last-name" class="form-control" placeholder="Enter Last Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">Telephone</label>
                    <input type="tel" required="required" id="telephone" name="telephone" class="form-control" placeholder="Enter Telephone Number" />
                </div>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="submit" >Next</button>
            </div>
        </div>
    </div>
</form>