<form role="form" id="step-two" data-token="{{ csrf_token() }}">
    <div class="row setup-content">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 2</h3>
                <div class="form-group">
                    <label class="control-label">Street</label>
                    <input maxlength="200" type="text" required="required" id="street" name="street" class="form-control" placeholder="Enter Street Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">House Number</label>
                    <input maxlength="200" type="text" required="required" id="house" name="house" class="form-control" placeholder="Enter House Number"  />
                </div>
                <div class="form-group">
                    <label class="control-label">Zip Code</label>
                    <input maxlength="200" type="text" required="required" id="zip_code" name="zip_code" class="form-control" placeholder="Enter Zip Code"  />
                </div>
                <div class="form-group">
                    <label class="control-label">City</label>
                    <input maxlength="200" type="text" required="required" id="city" name="city" class="form-control" placeholder="Enter City Name"  />
                </div>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="submit" >Next</button>
            </div>
        </div>
    </div>
</form>