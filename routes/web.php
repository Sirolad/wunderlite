<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::post('step1', 'Auth\RegisterController@stepOne');

Route::post('step2', 'AddressController@stepTwo');

Route::post('step3', 'PaymentController@stepThree')->name('stepThree');

Route::get('success', 'PaymentController@success')->name('success');